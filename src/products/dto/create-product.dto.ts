import {
  IsNotEmpty,
  MinLength,
  IsPositive,
  IsNegative,
  IsNumber,
} from 'class-validator';
export class CreateProductDto {
  @MinLength(8)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNumber()
  @IsNotEmpty()
  price: number;
}
